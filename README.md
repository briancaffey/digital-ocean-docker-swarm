# Docker Swarm on Digital Ocean

This repo will demonstrate the deployment of a Django application on Digital Ocean using docker swarm. It will use the following technologies:

- Django
- Vue.js
- Postgres
- Redis
- nginx
- traefik
- GitLab CI

This project will not used managed postgres and redis databases. Instead, it will show how to use volumes to persist data for the redis and postgres databases used in this project. This will help keep costs down.

## Costs

The costs include:

- \$5/month basic Digital Ocean Droplet
- \$0.10/month 25GB volume

This project will configure our digital ocean droplet to use DNS to connect to a free `.tk` domain.

Additionally, the project will use SSL certificates from Let's Encrypt using CertBot through traefik.

## Setup

- [x] Spin up DigitalOcean Droplet with SSH key
- [x] Add `SSH_PRIVATE_KEY` to GitLab environment variables
- [x] Add Droplet IP as `DROPLET_IP` environment variable
- [x] Create a DigitalOcean Personal Access Token
- [x] Configure rex-ray addon as described here using DO personal access token: [https://www.digitalocean.com/community/questions/how-to-attach-digitalocean-block-storage-to-docker-container](https://www.digitalocean.com/community/questions/how-to-attach-digitalocean-block-storage-to-docker-container)
- [x] Create public traefik network manually
- [x] Set environment variables in GitLab > Settings > CI/CD > Variables including POSTGRES_PASSWORD
- [x] Login to the GitLab private registry using environment variables
- [x] Build and tag images in GitLab CI to be used
- [x] Django image
- [x] Nginx image
- [x] Traefik image - [https://blog.creekorful.com/2019/10/how-to-install-traefik-2-docker-swarm/](https://blog.creekorful.com/2019/10/how-to-install-traefik-2-docker-swarm/)
- [x] Setup static and
- [ ] media asset files storage
- [x] Setup A record for existing Route53 domain
- [x] Confirm that SSL is working
- [ ] Add model with FileField field to test media uploads
- [x] Add Django application with static files and database connection
- [x] Create and run management commands, trigger from GitLab CI
- [ ] Add Vue.js application and add as build stage in nginx Dockerfile
- [ ] Add notes on local setup for Django/Vue development